import React from 'react';
import {
  Routes,
  Route
} from 'react-router-dom';
import './App.css';
import Login from './Pages/LoginPage/Login';
import Registration from './Pages/RegistrationPage/Registration';
import Rating from './Pages/RatingPage/Rating';
import GroupedSelect from './Pages/DropDownPage/DropDown';
import { Average } from './Pages/AveragePage/Average';

function App() {
  return (
    <>
      <Routes>
        <Route path='/' element={<Registration />}></Route>
        <Route path='loginPage' element={<Login />}></Route>

        <Route path="average" element={<Average />}></Route>

        <Route path="rating" element={<Rating />}></Route>
        <Route path="groupedSelect" element={<GroupedSelect />}></Route>
      </Routes>
    </>
  );
}

export default App;
