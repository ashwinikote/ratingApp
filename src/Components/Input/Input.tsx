import styles from "./Input.module.scss";
import { TextField } from "@mui/material";


const Input = (props: any) => (
    <div className={styles.input}>
        <TextField {...props} variant="outlined" />
        { props.error && <span className={styles.error}>{ props.label } is invalid</span> }
    </div>
);

export default Input;