import React from 'react';
import { Button as MButton } from '@mui/material';

const Button = ({ children, ...props }: React.PropsWithChildren<any>) => {
    return <MButton {...props} variant="contained">{children}</MButton>
}

export default Button;