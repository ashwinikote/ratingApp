import { Avatar, Paper, Typography } from '@mui/material';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Button from '../../Components/Button/Button';
import Input from '../../Components/Input/Input';
import { register } from '../../Services/auth.services';
import styles from './Registration.module.scss';
import AccountCircleRoundedIcon from '@mui/icons-material/AccountCircleRounded';
import { width } from '@mui/system';

const input_style = {
  marginBottom: "15px"
}

//import { Link } from 'react-router-dom';
const Registration = () => {

  const [form, setForm] = useState({
    email: {
      value: '',
      touched: false,

      get valid() {
        if (!this.touched) return true;

        const pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,20}$/;

        if (!this.value.trim() || !pattern.test(this.value)) {
          return false;
        }

        return true;

      }
    },

    password: {
      value: '',
      touched: false,

      get valid() {
        if (!this.touched) return true;

        if (!this.value.trim()) {
          return false;
        }
        return true;
      }
    },

    name: {
      value: '',
      touched: false,

      get valid() {
        if (!this.touched) return true;

        if (!this.value.trim()) {
          return false;
        }
        return true;
      }
    },

  });

  const onFormInputChange = (value: string, field: 'email' | 'password' | 'name') => {
    const formClone = { ...form };
    form[field].value = value;
    setForm(formClone);
  }

  const onFormInputFocus = (field: 'email' | 'password' | 'name') => {
    const formClone = { ...form };
    setForm(formClone);
  }

  const onFormSubmit = async () => {
    const formValues = {
      name: form.name.value,
      email: form.email.value,
      password: form.password.value
    };

    const formClone = { ...form };
    formClone.name.touched = false;
    formClone.name.value = '';
    formClone.email.touched = false;
    formClone.email.value = '';

    formClone.password.touched = false;
    formClone.password.value = '';
    setForm(formClone);

    await register(formValues)

  }

  return (
    <div className={styles.home}>


      <main className={styles.main}>

        <div className={styles.formContainer}>

          <Paper elevation={3} className={styles.paper}>
            <Avatar sx={{ bgcolor: '#1976d2' }}>
              <AccountCircleRoundedIcon />
            </Avatar>
            <br />
            <form>
              <Input
                label="Name"
                error={!form.name.valid}
                onFocus={() => onFormInputFocus('name')}
                value={form.name.value}
                onChange={(e: any) => onFormInputChange(e.target.value, 'name')}
                sx={input_style}
              />
              <Input
                label="Email"
                error={!form.email.valid}
                onFocus={() => onFormInputFocus('email')}
                value={form.email.value}
                onChange={(e: any) => onFormInputChange(e.target.value, 'email')}
                sx={input_style}
              />
              <Input
                label="Password"
                type="password"
                onFocus={() => onFormInputFocus('password')}
                error={!form.password.valid}
                value={form.password.value}
                onChange={(e: any) => onFormInputChange(e.target.value, 'password')}
                sx={input_style} />
              <div>
                <Button className={styles.loginBtn}
                  onClick={onFormSubmit}
                  disabled={!form.password.valid || !form.email.valid || !form.name.valid}
                  sx={{width:"100%"}}
                  >
                  Submit
                </Button>
              </div>
            </form>

            <Typography variant="overline">
              Already have an Account? <Link to="loginPage">Click Here</Link>
            </Typography>

          </Paper>

          {/* <div className={styles.link}>
            <Link to={"/loginPage"}>Login</Link>
          </div> */}

        </div>
      </main>
    </div>
  )
}

export default Registration
