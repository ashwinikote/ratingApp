import React from 'react';
import Rating from '@mui/material/Rating';
import Stack from '@mui/material/Stack';
import { useState } from 'react';
import { IRating } from "../RatingPage/Input";
import { RatingService } from '../../Services/auth.services';

export default function RatingSIze() {

  // {ambiance: Number, food: Number, service: Number, cleanliness: Number, drinks: Number}
  const[rating, setRating] = useState({});

  const handleChange =(e) => {
    console.log(e.target.name)
    const field = e.target.name;
    rating[String(field)] = Number(e.target.value)
    setRating({ ...rating })
  }

  const handleSubmit = async(e) => {
    e.preventDefault()
    console.log(rating)
    let resp = await RatingService(rating)
  }

  return (
    <main className='rmain'>
    <Stack spacing={1}>
      
      
      <Rating className='amb' name="ambiance" defaultValue={0} onChange={handleChange}  size="large" /> Ambiance 
      <Rating className='food'name="food" defaultValue={0} onChange={handleChange} size="large" />  Food
      <Rating className='ser' name="service" defaultValue={0} onChange={handleChange} size="large" /> Service
      <Rating className='clean' name="cleanliness" defaultValue={0} onChange={handleChange} size="large" /> Cleanliness
      <Rating className='drink' name="drink" defaultValue={0} onChange={handleChange} size="large" /> Drinks

    </Stack>

    <button onClick={handleSubmit}>Submit</button>

    </main>
  );
}