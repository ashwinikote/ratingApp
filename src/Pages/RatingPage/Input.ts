export interface IRating{
  ambiance: Number,
  food: Number, 
  service: Number,
  cleanliness: Number, 
  drink: Number
}