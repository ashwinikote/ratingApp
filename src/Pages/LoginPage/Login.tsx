import React, { useEffect } from 'react';
import { useState } from 'react';
import Button from '../../Components/Button/Button';
import Input from '../../Components/Input/Input';
import styles from './Login.module.scss';
import { ROLES } from './Login.constants';
import { login } from '../../Services/auth.services';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import { Alert, Avatar, Paper, Typography } from '@mui/material';
import AccountCircleRoundedIcon from '@mui/icons-material/AccountCircleRounded';

const input_style = {
  marginBottom: "20px"
}

const Login = () => {
  const navigate = useNavigate()

  const [error, setError] = useState("")

  const [form, setForm] = useState({
    email: {
      value: '',
      touched: false,

      get valid() {
        if (!this.touched) return true;

        const pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,20}$/;

        if (!this.value.trim() || !pattern.test(this.value)) {
          return false;
        }

        return true;

      }
    },

    password: {
      value: '',
      touched: false,

      get valid() {
        if (!this.touched) return true;

        if (!this.value.trim()) {
          return false;
        }
        return true;
      }
    },

  });

  const onFormInputChange = (value: string, field: 'email' | 'password') => {
    const formClone = { ...form };
    form[field].value = value;
    setForm(formClone);
  }

  const onFormInputFocus = (field: 'email' | 'password') => {
    const formClone = { ...form };
    setForm(formClone);
  }

  const onFormSubmit = async () => {
    const formValues = {
      email: form.email.value,
      password: form.password.value
    };

    const formClone = { ...form };
    formClone.email.touched = false;
    formClone.email.value = '';

    formClone.password.touched = false;
    formClone.password.value = '';
    setForm(formClone);

    let resp = await login(formValues);
    if (resp.status === 200) {
      localStorage.setItem('estimate-token', resp.data.data.token);
      if (resp.data.data.role === 1) {
        navigate("/groupedSelect")
      }
      else {
        navigate("/rating")
      }
    }
    else {
      setError("Invalid Credentials")
    }


  }
  return (
    <div className={styles.login}>
      <Paper elevation={3} className={styles.paper}>
        {error && <Alert severity="error">Invalid Credentials</Alert>}
        <Avatar sx={{ bgcolor: '#1976d2', margin: "10px" }}>
          <AccountCircleRoundedIcon />
        </Avatar>
        <form>
          <Input
            label="Email"
            error={!form.email.valid}
            onFocus={() => onFormInputFocus('email')}
            value={form.email.value}
            onChange={(e: any) => onFormInputChange(e.target.value, 'email')}
            sx={input_style}
          />

          <Input
            label="Password"
            type="password"
            onFocus={() => onFormInputFocus('password')}
            error={!form.password.valid}
            value={form.password.value}
            onChange={(e: any) => onFormInputChange(e.target.value, 'password')}
            sx={input_style}
          />

          <div>
            <Button
              className={styles.loginBtn}
              onClick={onFormSubmit}
              disabled={!form.password.valid || !form.email.valid}
              sx={input_style}>Login</Button>
          </div>
        </form>
        <Typography variant="overline">
          Don't have Account? <Link to="/">Click Here</Link>
        </Typography>
      </Paper>
    </div>
  )
}

export default Login
