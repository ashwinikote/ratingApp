import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import ListSubheader from '@mui/material/ListSubheader';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { useState } from 'react';
import { Button } from '@mui/material';
import { Link } from 'react-router-dom';
import { IRating } from '../RatingPage/Input';
import { Filter } from '../../Services/auth.services';

const DropDown = ({ name, filter, setFilter }) => {


  const handleChange = (event) => {
    filter[event.target.name.toLowerCase()] = Number(event.target.value)
    setFilter({ ...filter })
    console.log(filter)
  }

  <Link to={"/average"}>View</Link>

  return (


    <FormControl sx={{ m: 1, minWidth: 120 }}>

      <InputLabel htmlFor="grouped-native-select">{name}</InputLabel>
      <Select native defaultValue="" id="grouped-native-select" onChange={handleChange} name={name} label={name}>
        <option aria-label="None" value="" />

        <option value={1}> 1 </option>
        <option value={2}> 2 </option>
        <option value={3}> 3 </option>
        <option value={4}> 4 </option>
        <option value={5}> 5 </option>

      </Select>
    </FormControl>



  )
}
export default function GroupedSelect() {
  const [filter, setFilter] = useState({ ambiance: 0, food: 0, service: 0, cleanliness: 0, drinks: 0 })
  const [data, setData] = useState([])

  const handleSubmit = async (e) => {
    e.preventDefault()
    let resp = await Filter(filter)
    if(!resp.data.message)
      setData(resp.data)
    else
      alert(resp.data.message)
    console.log(resp)
  }

  return (
    <>
      <div>
        <DropDown name="Ambiance" filter={filter} setFilter={setFilter} />
        <DropDown name="Food" filter={filter} setFilter={setFilter} />
        <DropDown name="Service" filter={filter} setFilter={setFilter} />
        <DropDown name="Cleanliness" filter={filter} setFilter={setFilter} />
        <DropDown name="Drinks" filter={filter} setFilter={setFilter} />
        <Button onClick={handleSubmit}>Submit</Button>
      </div >
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Ambiance</th>
            <th>Food</th>
            <th>Service</th>
            <th>Cleanliness</th>
            <th>Drinks</th>
          </tr>
        </thead>
        <tbody>
          {
            data && data.map((row: any) => 
              <tr>
                <td>{row.name}</td>
                <td>{row.ambiance}</td>
                <td>{row.food}</td>
                <td>{row.service}</td>
                <td>{row.cleanliness}</td>
                <td>{row.drink}</td>
              </tr>
            )
          }
        </tbody>
      </table>
    </>

  );
}
