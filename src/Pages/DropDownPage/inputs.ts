export interface IMySelect {
    fieldName: string,
    options: any,
    changed: any,
    option: any
}