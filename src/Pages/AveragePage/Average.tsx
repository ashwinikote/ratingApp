
import React, { useEffect, useState } from 'react';
import { getData, Header } from '../../Services/auth.services';



export const Average = () => {

  const [report, setReport] = useState({ ambiance: 0, food: 0, service: 0, cleanliness: 0, totalAvg: 0 })



  useEffect(() => {
    getData().then(data=>{
      setReport(data.data)
      console.log(data.data)
    })
  }, [])

  return (
    <div >

      <hr />
      <table className="table table-sm">
        <tr>
          <th>Param</th>
          <th>Rating</th>
        </tr>
        <tr>
          <td>Ambiance</td>
          <td>{report.ambiance}</td>
        </tr>
        <tr>
          <td>food</td>
          <td>{report.food}</td>
        </tr>
        <tr>
          <td>service</td>
          <td>{report.service}</td>
        </tr>
        <tr>
          <td>cleanliness</td>
          <td>{report.cleanliness}</td>
        </tr>
        <tr>
          <td>Total Avg</td>
          <td>{report.totalAvg}</td>
        </tr>
      </table>

    </div>

  )
}
