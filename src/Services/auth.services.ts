import { IRating } from "../Pages/RatingPage/Input";
import axios from "./axios.instance";
import { ICredentials, IUser } from "./services.types";

export const Header = {
    "Accept": "application/json, text/plain, */*",
    "Content-type": "application/json; charset=utf-8",
    "Authorization": `Bearer ${localStorage.getItem("token")}`
  }

export const login = async (credentials: ICredentials) => {
    let response = undefined
    try {
        response = await axios.post('api/auth/login', credentials);
        console.log(response)
        return response;
    } catch (e) {
        console.log('something went wrong',response);
        return {status:403}
    }
}

export const register = async (user: IUser) => {
    try {
        console.log(user)
        const response = await axios.post('api/auth/signup', user);
        return response.data;
    } catch (e) {
        // handle in a better way
        alert("Error From Backend"+e)
        console.log('something went wrong');
    }
}

export const RatingService = async (rating: any) => {
    try {
        console.log(rating)
        const response = await axios.post('api/rating',rating);
        return response.data;
    } catch (e) {
        // handle in a better way
        alert("Error From Backend"+e)
        console.log('something went wrong');
    }

}

export const Filter = async (rating: any) => {
    try {
        const response = await axios.get('admin/filter', {params:rating});
        return response.data;
    } catch (e) {
        // handle in a better way
        console.log('something went wrong');
    }
}

export const getData = async () => {
    try {

        const response = await axios.get('admin/reports')
        return await response.data;
    } catch (e) {
        // handle in a better way
        console.log('something went wrong');
    }
}