export interface ICredentials {
    email: string;
    password: string;

}

export interface IUser {
    name: string;
    email: string;
    password: string;
}

