import axios from "axios";

const BaseURL = 'https://0594-103-51-153-190.in.ngrok.io/'
const axiosInstance = axios.create({
    baseURL: BaseURL
});

axiosInstance.interceptors.request.use((config) => {
    const token = localStorage.getItem('estimate-token');
    config.headers = {
        Authorization: "Bearer "+token || ''
    }
    return config;
})

export default axiosInstance;